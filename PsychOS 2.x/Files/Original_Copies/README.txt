The following files are replaced in PsychOS > 2.6.1 to make user experience
more pleasant and increase security. ~/ refers to the home folder. ~/.
locations are hidden. To unhide these folders, press Ctrl+h and press
the key combo again to hide them when done. Backups of these files are made
just in case you had you own custom settings you don't want to lose.
You can replace the "corrected" files with the backups by using:

sudo cp [drag and drop backup] [enter location to replace]

You can also use "su" followed by "thunar" in the terminal to open a
file manager to drag and drop as a superuser.

<<<[Original File locations]>>>
~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml
/etc/icewm/keys
/etc/icewm/preferences
/etc/icewm/toolbar
/etc/ssh/sshd_config
/etc/php5/cli/php.ini
/etc/php5/apache2/php.ini
~/.local/share/radiotray/bookmarks.xml
