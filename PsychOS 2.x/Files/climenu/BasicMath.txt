#######################
# Basic Math Formulas #
#######################

+ Fractions +
+++++++++++++
	| Adding Fractions
	------------------
		(a/b) + (c/d) = (ad + bc)÷(bd)
	                   
	| Subtracting Fractions
	-----------------------
		(a/b) - (c/d) = (ad - bc)÷(bd)
					   
	| Multiplying Fractions
	-----------------------
		(a/b) × (c/d) = (ac)÷(bd)
	
	| Dividing Fractions
	--------------------
		(a/b) ÷ (c/d) = (a/b) × (d/c) = (ad)÷(bc)
		
+ Geometry +
++++++++++++
	| Perimeter
	-----------
		Square: s + s + s + s
			[s: side]
		
		Rectangle: l + w + l + w
			[l: length]
			[w: width]
		
		Triangle: a + b + c
			[a,b,c: sides]
		
	| Area
	------
		Square: s × s
			[s: side]
		
		Rectangle: l × w
			[l: length]
			[w: width]
		
		Triangle: (b × h)÷2
			[b: base of triangle]
			[h: height]
		
		Trapezoid: (b1 + b2) × h÷2
			[b1: length of base 1]
			[b2: length of base 2]
			[h: height]
		
	| Volume
	--------
		Cube: s × s × s
			[s: side]
		
		Box: l × w × h
			[l: length]
			[w: width]
			[h: height]
		
		Sphere:  (4/3) × π × r³
			[π: 3.14]
			[r: radius of sphere]
			
		Triangular Prism: (1/2(b) × h) × h
			[b: base]
			[h: height]
			
		Cylinder: π × r² × h
			[π: 3.14]
			[r: radius]
			[h: height]
