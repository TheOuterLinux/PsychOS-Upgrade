#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
HEIGHT=20
WIDTH=60
CHOICE_HEIGHT=20
BACKTITLE="Programs List --> Education"
TITLE="Education"
MENU="Choose one of the following options:"

OPTIONS=(1 "<-- Go back"
		 2 "Concal - consol calculator"
         3 "Formulas & reference materials"
         4 "Primes - generate prime numbers between given numbers"
         5 "Classic literature"
         6 "Google Scholar search"
         7 "gperiodic (Needs GUI) - Periodic Table Information Lookup"
         8 "Your Sky - online star and planet maps"
         9 "SC-IM - open/edit spreadsheets"
         10 "gnuplot - plot a graph in the terminal"
         11 "jed - A text editor."
         12 "unit - convert measurements and currencies (2014)")

CHOICE_EDUCATIONMENU=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE_EDUCATIONMENU in
        1)
            cd $DIR
            ./programs-list
            ;;
        2)	while :
			do
				if input=$(dialog --inputbox "Enter calculation. Use SHIFT+CTRL+V to paste." 10 75 3>&1 1>&2 2>&3)
				then
					output=$(concalc "$input")
					dialog --msgbox "Answer: $output" 10 75
				else
					cd $DIR
					./education-list
				fi
			done
            cd $DIR
			./education-list
			exec 3>&-
            ;;
        3)
			cd $DIR
			./formulas
			;;
		4)	while :
			do
				error="1"
				while [ $error = "1" ]
				do
					if startnumber=$(dialog --inputbox "Enter start number." 10 75 3>&1 1>&2 2>&3)
					then
						if [[ $startnumber = +([0-9]) ]]
						then
							error="0"
						else
							dialog --msgbox "$startnumber is not a positive whole number. Please try again." 6 60
						fi
					else
						cd $DIR
						./education-list
					fi
				done
				error="1"
				while [ $error = "1" ]
				do
					if endnumber=$(dialog --inputbox "Start number is $startnumber. What is the finish number?" 10 75 3>&1 1>&2 2>&3)
					then
						if [[ $endnumber = +([0-9]) ]]
						then
							error="0"
						else
							dialog --msgbox "$endnumber is not a positive whole number. Please try again." 6 60
						fi
					else
						cd $DIR
						./education-list
					fi
				done
				output="$(primes $startnumber $endnumber)"
				echo "$output" > /tmp/primenumberslog.txt
				dialog --title "Prime Numbers from $startnumber to $endnumber" --textbox /tmp/primenumberslog.txt 32 80
			done
            cd $DIR
			./education-list
			exec 3>&-
            ;;
		5)
			if choice=$(dialog --backtitle "$BACKTITLE --> Classic Literature" --title "Please choose an option" --menu "" 12 40 20 \
				1 "<-- Go back" \
				2 "Bible" \
				3 "Gutenburg book search" 3>&1 1>&2 2>&3)
			then
				case $choice in
					1)
						cd $DIR
						./education-list
						;;
					2)
						w3m https://www.fourmilab.ch/etexts/www/Bible/Bible.html
						;;
					3)
						if search=$(dialog --inputbox "| Gutenburg book search. Use SHIFT+CTRL+V to paste. \n ------------------------------------------------------- \n SUFFIXES: \n a. for author \n t. for title \n s. for subject" 10 75 3>&1 1>&2 2>&3)
						then
							correctedsearch="${search// /+}"
							w3m "https://gutenberg.org/ebooks/search/?query=$correctedsearch"
						else
							cd $DIR
							./education-list
						fi
						cd $DIR
						./education-list
						;;
				esac
			else
				cd $DIR
				./image-edit
			fi
			cd $DIR
			./education-list
			exec 3>&-
            ;;
        6)
			rm -rf /tmp/googlescholarlog.txt
			cd $DIR/scholar.py-master
			exec 3>&1
			author=""
			phrase=""
			if search=$(dialog --ok-label "Submit" --backtitle "$BACKTITLE -> Google Scholar Search" --title "Search parameters" --form "Not all boxes need to be filled." 10 75 0 \
					"Author:" 1 1 "$author" 1 10 60 0 \
					"Phrase:" 2 1 "$phrase" 2 10 60 0 \
					2>&1 1>&3)
			then
				exec 3>&-
				output="$(python scholar.py --author '$author' --phrase '$phrase')"
				echo "$output" > /tmp/googlescholarlog.txt
				dialog --title "Google Scholar Results" --textbox /tmp/googlescholarlog.txt 32 80
				rm -rf /tmp/googlescholarlog.txt
			else
				cd $DIR
				./education-list
			fi
			cd $DIR
			./education-list
			;;
        7)
			clear
			RED="\033[0;31m"
			ORANGE="\033[0;33m"
			YELLOW="\033[0;93m"
			GREEN="\033[0;32m"
			PURPLE="\033[0;35m"
			BLUE="\033[1;34m"
			LBLUE="\033[1;96m"
			NOBLE="\033[1;44m"
			LGRAY="\033[1;37m"
			DGRAY="\033[1;90m"
			NC="\033[0m"

echo -e "  .---.                                                               .---.
1 | ${BLUE}H${NC} |                                                               |${PURPLE}He${NC} |
  |---+---.                                       .-------------------+---|
2 |${RED}Li${NC} |${ORANGE}Be${NC} |                                       | ${LBLUE}B${NC} | ${BLUE}C${NC} | ${BLUE}N${NC} | ${BLUE}O${NC} | ${NOBLE}F${NC} |${PURPLE}Ne${NC} |
  |---+---|                   |----8B-----|       |---+---+---+---+---+---|
3 |${RED}Na${NC} |${ORANGE}Mg${NC} |3B  4B  5B  6B  7B v           v 1B 2B |${GREEN}Al${NC} |${LBLUE}Si${NC} | ${BLUE}P${NC} | ${BLUE}S${NC} |${NOBLE}Cl${NC} |${PURPLE}Ar${NC} |
  |---+---+---------------------------------------+---+---+---+---+---+---|
4 | ${RED}K${NC} |${ORANGE}Ca${NC} |${YELLOW}Sc${NC} |${YELLOW}Ti${NC} | ${YELLOW}V${NC} |${YELLOW}Cr${NC} |${YELLOW}Mn${NC} |${YELLOW}Fe${NC} |${YELLOW}Co${NC} |${YELLOW}Ni${NC} |${YELLOW}Cu${NC} |${YELLOW}Zn${NC} |${GREEN}Ga${NC} |${LBLUE}Ge${NC} |${LBLUE}As${NC} |${BLUE}Se${NC} |${NOBLE}Br${NC} |${PURPLE}Kr${NC} |
  |---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---|
5 |${RED}Rb${NC} |${ORANGE}Sr${NC} | ${YELLOW}Y${NC} |${YELLOW}Zr${NC} |${YELLOW}Nb${NC} |${YELLOW}Mo${NC} |${YELLOW}Tc${NC} |${YELLOW}Ru${NC} |${YELLOW}Rh${NC} |${YELLOW}Pd${NC} |${YELLOW}Ag${NC} |${YELLOW}Cd${NC} |${GREEN}In${NC} |${GREEN}Sn${NC} |${LBLUE}Sb${NC} |${LBLUE}Te${NC} | ${NOBLE}I${NC} |${PURPLE}Xe${NC} |
  |---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---|
6 |${RED}Cs${NC} |${ORANGE}Ba${NC} |${LGRAY}LAN${NC}|${YELLOW}Hf${NC} |${YELLOW}Ta${NC} | ${YELLOW}W${NC} |${YELLOW}Re${NC} |${YELLOW}Os${NC} |${YELLOW}Ir${NC} |${YELLOW}Pt${NC} |${YELLOW}Au${NC} |${YELLOW}Hg${NC} |${GREEN}Tl${NC} |${GREEN}Pb${NC} |${GREEN}Bi${NC} |${LBLUE}Po${NC} |${NOBLE}At${NC} |${PURPLE}Rn${NC} |
  |---+---+---+-----------------------------------------------------------
7 |${RED}Fr${NC} |${ORANGE}Ra${NC} |${DGRAY}ACT${NC}|${YELLOW}Rf${NC} |${YELLOW}Db${NC} |${YELLOW}Sg${NC} |${YELLOW}Bh${NC} |${YELLOW}Hs${NC} |${YELLOW}Mt${NC} |${YELLOW}Ds${NC} |${YELLOW}Rg${NC} |${YELLOW}Cn${NC} |${GREEN}Uut${NC}|${GREEN}Fl${NC} |${GREEN}Uup${NC}|${GREEN}Lv${NC} |${NOBLE}Uus${NC}|${PURPLE}Uuo${NC}|
   -----------------------------------------------------------------------
${RED}Alkali Metal${NC} ${ORANGE}Alkaline Earth${NC} ${YELLOW}Transition Metal${NC} ${GREEN}Basic Metal${NC} ${BLUE}Nonmetal${NC} ${LBLUE}Semimetal${NC}
${NOBLE}Halogen${NC} ${PURPLE}Noble Gas${NC} ${LGRAY}Lanthanide${NC} ${DGRAY}Actinide${NC}
              .-----------------------------------------------------------.
   Lanthanide |${LGRAY}La${NC} |${LGRAY}Ce${NC} |${LGRAY}Pr${NC} |${LGRAY}Nd${NC} |${LGRAY}Pm${NC} |${LGRAY}Sm${NC} |${LGRAY}Eu${NC} |${LGRAY}Gd${NC} |${LGRAY}Tb${NC} |${LGRAY}Dy${NC} |${LGRAY}Ho${NC} |${LGRAY}Er${NC} |${LGRAY}Tm${NC} |${LGRAY}Yb${NC} |${LGRAY}Lu${NC} |
              |---+---+---+---+---+---+---+---+---+---+---+---+---+---+---|
   Actinide   |${DGRAY}Ac${NC} |${DGRAY}Th${NC} |${DGRAY}Pa${NC} | ${DGRAY}U${NC} |${DGRAY}Np${NC} |${DGRAY}Pu${NC} |${DGRAY}Am${NC} |${DGRAY}Cm${NC} |${DGRAY}Bk${NC} |${DGRAY}Cf${NC} |${DGRAY}Es${NC} |${DGRAY}Fm${NC} |${DGRAY}Md${NC} |${DGRAY}No${NC} |${DGRAY}Lw${NC} |
.              ----------------------------------------------------------- "
			echo ""
            echo -n "Enter element abbreviation from the Periodic Table and press ENTER:"
            read element
            elementinfo=$(gperiodic $element)
            echo "$elementinfo" > /tmp/elementlog.txt
            dialog --title "Information for element '$element'" --textbox /tmp/elementlog.txt 32 80
            rm -rf /tmp/elementlog.txt
            cd $DIR
			./education-list
			exec 3>&-
            ;;
        8)
			cd /tmp/
			w3m https://www.fourmilab.ch/yoursky/
            cd $DIR
			./education-list
			exec 3>&-
            ;;
        9)
			cd ~/Documents
			scim
            cd $DIR
			./education-list
			exec 3>&-
            ;;
        10)
			gnuplot
            cd $DIR
			./education-list
			exec 3>&-
            ;;
        11)
			cd ~/Documents
            jed
            cd $DIR
			./education-list
			exec 3>&-
            ;;
        12)
			dialog --title "Units history. (/tmp/unitslog.txt) Exit to continue" --textbox /tmp/unitslog.txt 32 80
			clear
			echo "Use CTRL+D to exit program."
			units --verbose --log /tmp/unitslog.txt
			cd $DIR
			./education-list
			exec 3>&-
            ;;
        255)
            cd $DIR
            ./programs-list
            ;;
esac
