#########
# Music #
#########

+ Definitions
+++++++++++++

	Staff or stave: a set of five lines and four spaces the music notes
		and symbols are printed on.

	----------------------
	----------------------
	----------------------
	----------------------
	----------------------
	
	Parts of a note:
		1. Head
		2. Stem
		3. Flag
		4. Beam
	

                        #####_
                        ######--- .
                        ###########|
                        #     `####| Flag
                        #        `#|
                        #
                  Stem  #
                        #
                        #
                ,/####. #
               /#######`#
   Note Head  |##########
               \#########
                `.####,'
                  `''


                            ###    Beam
                            ######
                            ###########
                            #     `#######
                            #        `####
                            #            #
                      Stem  #            #
                            #            #
                            #            #
                    ,/####. #    ,/####. #
                   /#######`#   /#######`#
       Note Head  |##########  |##########
                   \#########   \#########
                    `.####,'     `.####,'
                      `''          `''

	Pitch: the relative highness or lowness of a sound.
		- a note head higher in the staff is higher pitch
		- a note head lower in the staff is lower pitch

	Clefs: symbols that determine which note letter names belong
		to the lines and spaces in a staff.

		Treble clef (G clef): a clef that wraps around the
			the second line from the bottom (the G note).

                                  .MMNM2
                                 .MMdT8M.
                                 MND    b	  G
                                 MF     M
	-------------------------M-----JM---------F-----------------
                                 M    JMM
                                 M  .JMMF
                                 4,.NMMF          E
                                 JMMMN@
	-----------------------JNMMMMD------------D-----------------
                              MMMMM@!
                            dMMMMY`N		  C 
                           MMMMP   J
	------------------#MM#-----JN&xu----------B-----------------
                         MMMF    .MNMMMMMMN.
                         NM@   .MNNMB@YWMMMMp     A
                        JMM!   JM#^  M    WMMx    
                         MM    WMi   Jb    NMb
	-----------------JMx----M.----N ---JNF----G NOTE------------
                          JM.    Tx.  4,   MB
                            Tm.       Jb .MY	  F 
                              ?BQ&&&&&&MT^
	-------------------------------JF---------E-----------------
                                        M
                                        Jr	  D
                              JMMNNN    .b
                             JMMMMMM    .F
                              MMNM#^   .M^
                               TMa....dD

		Bass clef (F clef): a clef that indicates the
			fourth line from the bottom of the staff,
			between two dots as the F note.

	
	---------------------------------------------------A--------
                        ...NMMMMMMMMMMMMa..
                    ..MMM#Y7        WMMMMMMMN..
                  .JMM5               JMMMMMMMMN.
         G      .MMM5                   TMMMMMMMMN.        .MMMMN.
               .MMM^                     JMMMMMMMMMx      JMMMMMMMb
              .MMMh.   ...                4MMMMMMMMMb     MMMMMMMMM
              MMMMMMMMMMMMMMa.             MMMMMMMMMML    JMMMMMMM5
             JMMMMMMMMMMMMMMMMp            4MMMMMMMMMM.     7YY5^
             JMMMMMMMMMMMMMMMMM2           JMMMMMMMMMMh
	-----.MMMMMMMMMMMMMMMMMh-----------JMMMMMMMMMMM----F NOTE----
              ?MMMMMMMMMMMMMMM#            JMMMMMMMMMMM
               .YMMMMMMMMMMMMD             JMMMMMMMMMMM
                  ?YWMMMM#Y^               MMMMMMMMMMM@    .MMMMN.
                                          .MMMMMMMMMMM$   JMMMMMMMb
                                          MMMMMMMMMMM@    MMMMMMMMM
                     E                   JMMMMMMMMMMM!    JMMMMMMM5
                                        .MMMMMMMMMMM5       ?YWY^
                                       .MMMMMMMMMMM$
                                      .MMMMMMMMMMM5
                                     .MMMMMMMMMMM
	----------------------------.MMMMMMMMMM------------D--------
                                   .MMMMMMMMMD
                                 .MMMMMMMMMD
                               ..MMMMMMMMF
                              .MMMMMMMM5		   C
                           ..MMMMMMM#^
                         ..MMMMMMM5
                       .MMMMMM#5
                    .&MMMMM#^
                   MMMMM#i
	--------MMMMW--------------------------------------B--------
             MMM#
            #5!



							   A



	---------------------------------------------------G--------

	Accidentals: symbols that make a slight change to the pitch
		of a note and the same notes that follow unless
		canceled by another accidental.

		♯ = sharp sign --> raises the pitch of a note
					one-half-step higher.

		♭  = flat sign --> lowers the pitch of a note
					one-half-step lower.

		♮ = natural sign --> cancels a previous sharp or
					flat sign.

	Bar Line: short vertical lines that divide staves into measures.
	
	Double Bar Line: two short vertical lines that end the music.

	Measure: the space between bar lines.

	𝄆 𝄇  = left and right repeat signs.
		𝄆 𝄇  --> play what is in between the left and right repeat
			signs again.
		𝄇 --> no left repeat sign means go back to the beginning.

	𝅝 = whole note
	𝅗𝅥 = half note
	𝅘𝅥 = quarter note
	𝅘𝅥𝅮 = eighth note
	𝅘𝅥𝅯 = sixteenth note
	𝅘𝅥𝅰 = thirty-second note
	𝅘𝅥𝅱 = sixty-fourth note
	𝅘𝅥𝅲 = one hundred twenty-eighth note
	𝄻 = whole rest
	𝄼 = half rest
	𝄽 = quarter rest
	𝄾 = eighth rest
	𝄿 = sixteenth rest
	𝅀 = thirty-second rest
	𝅁 = sixty-fourth rest
	𝅂 = one hundred twenty-eighth rest

	𝅘𝅥𝅮+𝅘𝅥𝅮 = ♫
	𝅘𝅥𝅯+𝅘𝅥𝅯 = ♬

	𝅘𝅥. = dotted quarter note: 1½ beats
	𝅗𝅥.= dotted half note: 3 beats
	𝅝. = dotted whole note: 6 beats

+ Time Signature
++++++++++++++++
	Tells how many beats per measure.
	4/4 is four beats per measure
	3/4 is three beats per measure
	2/4 is two beats per measure

+ Dynamics
++++++++++
	pp = pianissimo --> very soft
	p = piano --> soft
	mp = mezzo piano --> medium soft
	mf = mezzo forte --> medium loud
	f = forte --> loud
	ff = fortissimo --> very loud

+ PIANO
+++++++
 
   D♭     E♭       G♭    A♭    B♭       D♭    E♭       G♭    A♭    B♭
   C♯     D♯       F♯    G♯    A♯       C♯    D♯       F♯    G♯    A♯
------------------------------------------------------------------------
| |###|  |##|  |  |##|  |##|  |##|  |  |##|  |##|  |  |##|  |###| |###||
| |###|  |##|  |  |##|  |##|  |##|  |  |##|  |##|  |  |##|  |###| |###||
| |###|  |##|  |  |##|  |##|  |##|  |  |##|  |##|  |  |##|  |###| |###||
| |###|  |##|  |  |##|  |##|  |##|  |  |##|  |##|  |  |##|  |###| |###||
| |###|  |##|  |  |##|  |##|  |##|  |  |##|  |##|  |  |##|  |###| |###||
| |###|  |##|  |  |##|  |##|  |##|  |  |##|  |##|  |  |##|  |###| |###||
| |###|  |##|  |  |##|  |##|  |##|  |  |##|  |##|  |  |##|  |###| |###||
| `--'   `--'  |  `--'  `--'  `--'  |  `--'  `--'  |  `--'  `---' `---'|
|   |     |    |    |     |    |    |    |    |    |    |     |    |   |
|   |     |    |    |     |    |    |    |    |    |    |     |    |   |
| C |  D  | E  | F  |  G  | A  | B  | C  | D  |  E | F  |  G  | A  | B |
|   |     |    |    |     |    |    |    |    |    |    |     |    |   |
L___L_____L____L____L_____L____L____L____L____L____L____L_____L____L___|

+ Guitar
++++++++

(Standard Tuning)

   E|---F|--Gb|---G|--Ab|---A|--Bb|---B|---C|--Db|---D|--Eb|---E|
   B|---C|--Db|---D|--Eb|---E|---F|--Gb|---G|--Ab|---A|--Bb|---B|
   G|--Ab|---A|--Bb|---B|---C|--Db|---D|--Eb|---E|---F|--Gb|---G|
   D|--Eb|---E|---F|--Gb|---G|--Ab|---A|--Bb|---B|---C|--Db|---D|
   A|--Bb|---B|---C|--Db|---D|--Eb|---E|---F|--Gb|---G|--Ab|---A|
   E|---F|--Gb|---G|--Ab|---A|--Bb|---B|---C|--Db|---D|--Eb|---E|
                 O         O         O         O             OO  

   E|---F|--F#|---G|--G#|---A|--A#|---B|---C|--C#|---D|--D#|---E|
   B|---C|--C#|---D|--D#|---E|---F|--F#|---G|--G#|---A|--A#|---B|
   G|--G#|---A|--A#|---B|---C|--C#|---D|--D#|---E|---F|--F#|---G|
   D|--D#|---E|---F|--F#|---G|--G#|---A|--A#|---B|---C|--C#|---D|
   A|--A#|---B|---C|--C#|---D|--D#|---E|---F|--F#|---G|--G#|---A|
   E|---F|--F#|---G|--G#|---A|--A#|---B|---C|--C#|---D|--D#|---E|
                 O         O         O         O             OO  

+ Bass
++++++++

(Standard Tuning)

   G|--Ab|---A|--Bb|---B|---C|--Db|---D|--Eb|---E|---F|--Gb|---G|
   D|--Eb|---E|---F|--Gb|---G|--Ab|---A|--Bb|---B|---C|--Db|---D|
   A|--Bb|---B|---C|--Db|---D|--Eb|---E|---F|--Gb|---G|--Ab|---A|
   E|---F|--Gb|---G|--Ab|---A|--Bb|---B|---C|--Db|---D|--Eb|---E|
   B|---C|--Db|---D|--Eb|---E|---F|--Gb|---G|--Ab|---A|--Bb|---B|
                 O         O         O         O             OO  

   G|--G#|---A|--A#|---B|---C|--C#|---D|--D#|---E|---F|--F#|---G|
   D|--D#|---E|---F|--F#|---G|--G#|---A|--A#|---B|---C|--C#|---D|
   A|--A#|---B|---C|--C#|---D|--D#|---E|---F|--F#|---G|--G#|---A|
   E|---F|--F#|---G|--G#|---A|--A#|---B|---C|--C#|---D|--D#|---E|
   B|---C|--C#|---D|--D#|---E|---F|--F#|---G|--G#|---A|--A#|---B|
                 O         O         O         O             OO  

+ Mandolin/Violin (Violins don't have frets)
++++++++++++++++++++++++++++++++++++++++++++

(Standard Tuning)

   E|---F|--Gb|---G|--Ab|---A|--Bb|---B|---C|--Db|---D|--Eb|---E|
   A|--Bb|---B|---C|--Db|---D|--Eb|---E|---F|--Gb|---G|--Ab|---A|
   D|--Eb|---E|---F|--Gb|---G|--Ab|---A|--Bb|---B|---C|--Db|---D|
   G|--Ab|---A|--Bb|---B|---C|--Db|---D|--Eb|---E|---F|--Gb|---G|
                 O         O         O              O        OO 
