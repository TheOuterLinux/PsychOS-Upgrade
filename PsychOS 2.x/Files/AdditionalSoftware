#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
HEIGHT=20
WIDTH=70
CHOICE_HEIGHT=20
BACKTITLE="Main Menu --> Additional Software and Settings"
TITLE="Additional Software and Settings"
MENU="Choose one of the following options:"

OPTIONS=(1 "<-- Go back"
		 2 "Change frequency scaling to performance; maximize CPU clock rate"
         3 "Tor Browser (US) -> Browse the web privately."
         4 "Pale Moon -> An alternative to Firefox for older systems"
         5 "Emulator Bundle -> Video game and system emulators"
         6 "Linux game suggestions"
         7 "Gourmet -> A recipe manager with lots of tools and plugins."
         8 "Reditr -> A desktop Reddit client for Linux."
         9 "PeaZip -> A free archive manager that supports 150+ archive types."
         10 "VirtualBox -> Used to run an operating system while still in the current one."
         11 "Corebird -> Gtk+ Twitter client"
         12 "JavE -> Convert pictures to ascii art or create from scratch."
         13 "HubiC command line client -> A France-based cloud storage service."
         14 "Tresorit client -> Encrypted cloud storage with Swiss privacy laws."
         15 "Wireshark -> monitor network traffic"
         16 "lolcat -> cat command, but with a rainbow & animation options."
         17 "xpaint -> 1994 styled paint editor; very lightweight and easy to use.")

CHOICE_ADDITIONALSOFTWARE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE_ADDITIONALSOFTWARE in
        1)
            cd $DIR
            ./Upgrade
            exec 3>&-
            ;;
        2)
			sudo echo "Your frequency scaling settings:"
			sudo cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
			echo "Frequency scaling will be changed to performance. This means,"
			echo "your CPU will be able to run at its maximum clock rate."
			echo "Do so only at your own risk. Copies of original files with"
			echo "settings will be saved to $DIR/Files/Original_Copies/"
			echo "If you wish to continue, press ENTER. Ctrl+c to cancel."
			read ""
			sudo cp /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor $DIR/Original_Copies/
			sudo echo -n performance | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
        3)
			cd $DIR
			echo "Installing Tor Web Browser..."
			version_A="7"
			version_B="0"
			version_C="10"
			echo "Looking for latest available version, starting with $version_A.$version_B.$version_C. This may take a while."
			errorcheck=""
			while [ "$errorcheck" != "Not Found" ]
			do
				version_A=$((version_A+1))
				errorcheck=$(w3m https://www.torproject.org/dist/torbrowser/$version_A.$version_B.$version_C/ | grep "Not Found")
			done
			version_A=$((version_A-1))
			echo "Found $version_A.x.x"
			seconderror=""
			while [ "$seconderror" != "Not Found" ]
			do
				version_B=$((version_B+1))
				seconderror=$(w3m https://www.torproject.org/dist/torbrowser/$version_A.$version_B.$version_C/ | grep "Not Found")
			done
			version_B=$((version_B-1))
			echo "Found $version_A.$version_B.x"
			thirderror=""
			while [ "$thirderror" != "Not Found" ]
			do
				version_C=$((version_C+1))
				thirderror=$(w3m https://www.torproject.org/dist/torbrowser/$version_A.$version_B.$version_C/ | grep "Not Found")
			done
			version_C=$((version_C-1))
			echo "Found $version_A.$version_B.$version_C"
			wget -c https://www.torproject.org/dist/torbrowser/$version_A.$version_B.$version_C/tor-browser-linux32-$version_A.$version_B."$version_C"_en-US.tar.xz
            wget -c https://www.torproject.org/dist/torbrowser/$version_A.$version_B.$version_C/tor-browser-linux32-$version_A.$version_B."$version_C"_en-US.tar.xz.asc
            gpg --keyserver pool.sks-keyservers.net --recv-keys 0x4E2C6E8793298290
            gpg --fingerprint 0x4E2C6E8793298290
            echo "Please take a moment to look at the above information."
            echo "The Tor Browser team signs Tor Browser releases."
            echo "The import key is 0x4E2C6E8793298290"
            echo "Please press ENTER to continue."
            read ""
            echo "."
            echo "."
			gpg --verify tor-browser-linux32-$version_A.$version_B."$version_C"_en-US.tar.xz.asc tor-browser-linux32-$version_A.$version_B."$version_C"_en-US.tar.xz
			echo "."
			echo "."
			echo "If there is a warning it is because you haven't assigned"
			echo "a trust index to this person. This means that GnuPG" 
			echo "verified that the key made that signature, but it's up"
			echo "to you to decide if that key really belongs to the developer."
			echo "The best method is to meet the developer in person and"
			echo "exchange key fingerprints."
			echo "."
			echo "Press ENTER to coninue."
			echo "At this point, you may cancel Tor Browser install with Ctrl+c."
			echo "The Tor Browser will be installed to your HOME directory."
			read ""
			tar -xvJf tor-browser-linux32-$version_A.$version_B."$version_C"_en-US.tar.xz --directory ~/
			rm -rf tor-browser-linux32-$version_A.$version_B."$version_C"_en-US.tar.xz
			echo "."
			echo "."
			echo "Password may be needed to install icon and .desktop file for applications menu."
			sudo rm -rf /usr/share/applications/start-tor-browser.desktop
			sudo rm -rf ~/.local/share/applications/start-tor-browser.desktop
			cd ~/tor-browser_en-US/
			./start-tor-browser.desktop --register-app
            echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
        4)
			clear
			echo "You will be asked to install Pale Moon web browser."
            echo "You do not have to, but is recommended if Midori is too"
            echo "light and Firefox is too heavy for your system. Think of"
            echo "as early Firefox before everything changed and started"
            echo "using too much RAM and CPU. Flash may not work, but most" 
            echo "of the internet uses HTML5 anyway.Pale Moon is updated by a"
            echo "small but dedicated community at palemoon.org."
            echo "Enter your password and press ENTER to continue."
            sh pminstaller.sh
            echo "Finished. Press ENTER to continue."
            read ""
            cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
            ;;
        5)
			clear
			echo "This will install emulators:"
			echo "* FCEUX -> NES"
			echo "* ZSNES -> SNES"
			echo "* Mupen64Plus -> N64"
			echo "* PCSXR -> PS1"
			echo "* VBAM -> GBA"
			echo "* Desmume -> NDS"
			echo "* DOSBox -> DOS emulator for playing games"
			echo "This also includes joystick packages."
			echo "NO GAMES WILL BE INCLUDED."
			echo "Press ENTER to continue or Ctrl+c to cancel."
			read ""
			sudo zypper rm http://download.opensuse.org/repositories/home:/giordanoboschetti:/emu/openSUSE_Tumbleweed/home:giordanoboschetti:emu.repo
			sudo zypper install fceux zsnes libmupen64plus2 mupen64plus mupen64plus-* mupen64plus-plugin-video-glide64mk2 mupen64plus-plugin-video-rice m64py pcsxr vbam vbam-lang xf86-input-joystick qjoypad dosbox
			cd $DIR/Files/
			sudo cp pcsxr.desktop /usr/share/applications/
			sudo cp zsnes.desktop /usr/share/applications/
			sudo cp m64py.desktop /usr/share/applications/
			sudo cp wxvbam.desktop /usr/share/applications/
			echo "Downloading and installing FCEUX from source..."
			version_A="2"
			version_B="2"
			version_C="3"
			echo "Check for latest version, starting with $version_A.$version_B.$version_C. This may take a while."
			errorcheck=""
			while [ "$errorcheck" != "404 Not Found" ]
			do
				version_A=$((version_A+1))
				errorcheck=$(w3m https://downloads.sourceforge.net/project/fceultra/Source%20Code/$version_A.$version_B."$version_C"%20src/fceux-$version_A.$version_B.$version_C.src.tar.gz | grep "404 Not Found")
			done
			version_A=$((version_A-1))
			echo "Found $version_A.x.x"
			seconderror=""
			while [ "$seconderror" != "404 Not Found" ]
			do
				version_B=$((version_B+1))
				seconderror=$(w3m https://downloads.sourceforge.net/project/fceultra/Source%20Code/$version_A.$version_B."$version_C"%20src/fceux-$version_A.$version_B.$version_C.src.tar.gz | grep "404 Not Found")
			done
			version_B=$((version_B-1))
			echo "Found $version_A.$version_B.x"
			thirderror=""
			while [ "$thirderror" != "404 Not Found" ]
			do
				version_C=$((version_C+1))
				thirderror=$(w3m https://downloads.sourceforge.net/project/fceultra/Source%20Code/$version_A.$version_B."$version_C"%20src/fceux-$version_A.$version_B.$version_C.src.tar.gz | grep "404 Not Found")
			done
			version_C=$((version_C-1))
			echo "Found $version_A.$version_B.$version_C"
			wget -c https://downloads.sourceforge.net/project/fceultra/Source%20Code/$version_A.$version_B."$version_C"%20src/fceux-$version_A.$version_B.$version_C.src.tar.gz | grep "404 Not Found"
			tar -xf fceux-$version_A.$version_B.$version_C.src.tar.gz
			cd fceux-$version_A.$version_B.$version_C
			scons
			sudo scons install
			sudo cp fceux.desktop /usr/local/share/applications/
			sudo ln -s /usr/local/share/pixmaps/fceux.png /usr/share/pixmaps/
			cd ..
			echo "Downloading and installing Desmume from source..."
			version_A="0"
			version_B="9"
			version_C="1"
			echo "Check for latest version, starting with $version_A.$version_B.$version_C. This may take a while."
			errorcheck=""
			while [ "$errorcheck" != "404 Not Found" ]
			do
				version_A=$((version_A+1))
				errorcheck=$(w3m https://downloads.sourceforge.net/project/desmume/desmume/$version_A.$version_B.$version_C/desmume-$version_A.$version_B.$version_C.tar.gz | grep "404 Not Found")
			done
			version_A=$((version_A-1))
			echo "Found $version_A.x.x"
			seconderror=""
			while [ "$seconderror" != "404 Not Found" ]
			do
				version_B=$((version_B+1))
				seconderror=$(w3m https://downloads.sourceforge.net/project/desmume/desmume/$version_A.$version_B.$version_C/desmume-$version_A.$version_B.$version_C.tar.gz | grep "404 Not Found")
			done
			version_B=$((version_B-1))
			echo "Found $version_A.$version_B.x"
			thirderror=""
			while [ "$thirderror" != "404 Not Found" ]
			do
				version_C=$((version_C+1))
				thirderror=$(w3m w3m https://downloads.sourceforge.net/project/desmume/desmume/$version_A.$version_B.$version_C/desmume-$version_A.$version_B.$version_C.tar.gz | grep "404 Not Found")
			done
			version_C=$((version_C-1))
			echo "Found $version_A.$version_B.$version_C"
			wget -c https://downloads.sourceforge.net/project/desmume/desmume/$version_A.$version_B.$version_C/desmume-$version_A.$version_B.$version_C.tar.gz | grep "404 Not Found"
			tar -xf desmume-$version_A.$version_B.$version_C.tar.gz
			cd desmume-$version_A.$version_B.$version_C
			./configure
			make
			sudo make install
			cd ..
			./AdditionalSoftware
			exec 3>&-
			;;
		6)
			echo "Root permissions needed to make next menu executable."
			sudo chmod +x ./AdditionalSoftwareGames
			./AdditionalSoftwareGames
			exec 3>&-
			;;
	    7)
			sudo zypper install gourmet
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		8)
			dialog --msgbox "You will run across a dependency error. Go ahead and choose to install Reditr anyway." 10 75
			sudo ln -s /usr/lib/libudev.so.1.4.0 /usr/lib/libudev.so.0
			sudo zypper install http://reditr.com/downloads/linux/reditr_i386.rpm
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		9)
			sudo zypper install peazip
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		10)	
			sudo zypper install virtualbox
			echo "VirtualBox is installed. To add USB supprt, install"
			echo "VirtualBox Extension Pack at https://www.virtualbox.org/wiki/Downloads"
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		11)
			sudo zypper install corebird
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		12)
			echo "Installing JavE..."
			mkdir "JavE"
			cd JavE
			wget -c http://www.jave.de/developer/jave_6.0_RC2.zip
			unzip jave_6.0_RC2.zip
			rm -rf jave_6.0_RC2.zip
			cd ..
			sudo cp -R $DIR/JavE /opt/
			sudo cp jave.desktop /usr/share/applications/
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		13) 
			echo "Downloading and installing HubiC..."
			version_A="2"
			version_B="1"
			version_C="0"
			version_D="53"
			echo "Check for latest version, starting with $version_A.$version_B.$version_C.$version_D This may take a while."
			errorcheck=""
			while [ "$errorcheck" != "En continuant votre navigation sur ce site, vous acceptez l'utilisation des" ]
			do
				version_A=$((version_A+1))
				errorcheck=$(w3m http://mir7.ovh.net/ovh-applications/hubic/hubiC-Linux/$version_A.$version_B.$version_C/ | grep "En continuant votre navigation sur ce site, vous acceptez l'utilisation des")
			done
			version_A=$((version_A-1))
			echo "Found $version_A.x.x"
			seconderror=""
			while [ "$seconderror" != "En continuant votre navigation sur ce site, vous acceptez l'utilisation des" ]
			do
				version_B=$((version_B+1))
				seconderror=$(w3m http://mir7.ovh.net/ovh-applications/hubic/hubiC-Linux/$version_A.$version_B.$version_C/ | grep "En continuant votre navigation sur ce site, vous acceptez l'utilisation des")
			done
			version_B=$((version_B-1))
			echo "Found $version_A.$version_B.x"
			thirderror=""
			while [ "$thirderror" != "En continuant votre navigation sur ce site, vous acceptez l'utilisation des" ]
			do
				version_C=$((version_C+1))
				thirderror=$(w3m http://mir7.ovh.net/ovh-applications/hubic/hubiC-Linux/$version_A.$version_B.$version_C/ | grep "En continuant votre navigation sur ce site, vous acceptez l'utilisation des")
			done
			version_C=$((version_C-1))
			echo "Found $version_A.$version_B.$version_C.x"
			fourtherror=""
			while [ "$fourtherror" != "En continuant votre navigation sur ce site, vous acceptez l'utilisation des" ]
			do
				version_D=$((version_D+1))
				fourtherror=$(w3m http://mir7.ovh.net/ovh-applications/hubic/hubiC-Linux/$version_A.$version_B.$version_C/hubiC-Linux-$version_A.$version_B.$version_C.$version_D-linux.tar.gz | grep "En continuant votre navigation sur ce site, vous acceptez l'utilisation des")
			done
			version_D=$((version_D-1))
			echo "Found $version_A.$version_B.$version_C.$version_D"
			wget -c http://mir7.ovh.net/ovh-applications/hubic/hubiC-Linux/$version_A.$version_B.$version_C/hubiC-Linux-$version_A.$version_B.$version_C.$version_D-linux.tar.gz
			tar -xf hubiC-Linux-*-linux.tar.gz
			cd hubic
			sudo make install
			echo "."
			echo "."
			echo "To start HubiC command line client, run 'hubic start'"
			echo "in a terminal. Then, you can use 'watch -n 1 hubic status'"
			echo "to monitor syncing. Use --help for more options."
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		14)
			echo "Information borrowed from FAQ page of tresorit.com:"
			echo "--------------------------------------------------"
			echo "*Tresorit uses end-to-end, AES-256 encryption," 
			echo "zero-knowledge sharing, and Swiss privacy." 
			echo "."
			echo "*Tresorit runs on multiple Microsoft Azure datacenters" 
			echo "located in the European Union (Holland and Ireland)." 
			echo "."
			echo "*Tresorit is FINRA and HIPAA compliant and offers "
			echo "HIPAA BAA addendums to enterprise customers."
			echo "."
			echo "*Disaster proof and protected by 24/7 physical security,"
			echo "datacenters are also compliant with HIPAA, ISO27001:2013"
			echo "and a host of other certifications."
			echo "."
			echo "Press ENTER to continue with installation or Ctrl+c to cancel."
			wget -c https://installerstorage.blob.core.windows.net/public/install/tresorit_installer.run
			sudo ./tresorit_installer.run
			echo "."
			echo "."
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		15)
			sudo zypper install wireshark-ui-gtk
			echo "."
			echo "."
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		16)
			wget https://github.com/busyloop/lolcat/archive/master.zip
			unzip master.zip
			cd lolcat-master/bin
			gem install lolcat
			echo "."
			echo "."
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
		17)
			sudo zypper install xpaint
			echo "."
			echo "."
			echo "Finished. Press ENTER to continue."
            read ""
			cd $DIR/Files/
			./AdditionalSoftware
			exec 3>&-
			;;
esac
