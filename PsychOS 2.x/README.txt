Distro: PsychOS                     |
Website: TheOuterLinux.com          |
Email: theouterlinux@protonmail.com |
------------------------------------

Author's Note:
-----------------------------------------------------------------------
PsychOS for almost a year has been based off of SuseStudio.com's build
services. However, they have recently decided to let 32-bit die-off.
This means, that all updates for the time being will be in the form of
scripts. This also means that over time, the Upgrade script will get
larger and larger over time, but hopefully not by much as PsychOS is 
very stable as it is already. Also, there are other repositories besides 
what SuseStudio and OpenSUSE uses that work almost exactly as theirs
does simply because they are cloned copies. As long as "bit rotting," as
a personal email was put, doesn't occur at any rapid rate, PsychOS
installed software should continue to update as usual for another few
years. Just know, this is not a guarantee. Builds of a 64-bit version
of PsychOS have been attempted many times using SuseStudio.com, but
continuously fail just as the 32-bit ones do.
-----------------------------------------------------------------------

You may need to sudo chmod +x Upgrade in order to run.

Upgrade script is divided as such:
----------------------------------
[Startup Animation]
* See folder /Files/Animation/
* Uses an if statement to check for file existence (played.txt) and
	contains data. The initial file played.txt contains only a
	"0." After running the Upgrade script, this file is deleted
	and the animation will not play again unless the another
	file named "played.txt" is placed inside /Files/Animation/.
	This prevents the animation from playing after each step in
	the upgrading process.

Step 1: 
* Repos that stopped working are removed
* Repos that are removed are replaced with alternatives
* Kernel repo added
* Python repo added
* Lynis RootKit checker repo added

Step 2:
* Update packages installed before installing any new ones
* Update Lynis RootKit checker by installing from Lynis repo
* Update clamav
* Update Mendeley
* Update Blender

Step 3:
* Install new packages: 
	kruler --> virtual ruler on screen
	extcalc --> graphing and statistics calculator
	ipscan --> scan network for devices connected
	fslint --> duplicate file finder
	acpi --> check battery stats in command line
	zenity --> pop-up messenger installed for purpose of notitrans
	xsel --> get text from what is highlighted for purpose of notitrans
	python-MySQL-python --> added just in case a Python developer wants to
				make an application that uploads data to a
				MySQL database. I may make a bug reporting app
				one of these days and will need these libraries
				installed to make them work.
	python-PyMySQL
	keepassx --> Use it to store passwords, URL's, notes, or files. KeePassX
					is also available for Windows and Mac.
	librsvg-devel --> Added just in case of JWM installation
	fribidi-devel --> Added just in case of JWM installation
	xev --> A utility to help map out the keyboard, mouse, or joystick.
	gnome-sound-recorder --> A simple to use audio recorder
	filezilla --> FTP file manager (from web)
	fontforge --> Create your own custom fonts or edit others
	trash-cli --> Manage trash in command line mode
	python-Levenshtein --> May be needed for future app launchers
	scrot --> Easier way to screenshot from command line if needed
	synapse --> Find both applications and files much more easily. Synapse
		is supported on other desktop environments and Whisker Menu needs
		the xfce4-panel to work. Plus, Synapse only uses ~6MB or RAM.
	pdftk-qgui --> Edit, slice and dice PDF files, and so forth. Run pdftk
		in command line as well.
	powertop --> Diagnose issues related to power consumption and management.
	gnuplot --> Fast wat to plot graphs in terminal or qt and export as an image.
	asciinema --> Record terminal tty sessions like a gui-less screen recorder.
	TermFeed --> RSS news aggregation
	python3-dbm --> Dependency for TermFeed
	mailx --> Command line email management
	bsd-games --> Text-based games for climenu Games section
	frotz --> An interpreter for text-based games like Zork.
		*https://www.infocom-if.org/games/games.html
	python-wxGlade --> GUI builder for Python
	exiftool --> Read/edit/wipe metadata inside images.
	iptraf-ng --> CLI Monitor network traffic
	iftop --> CLI Monitor network traffic (bandwidth)
	taskwarrior --> CLI task manager
	traceroute --> traces sent packets to server destination
	espeak --> Voice synthesizer for command line
	tcpdump --> Monitor network traffic
	dtrx --> Intelligent archive utility
	wxhexeditor --> GUI hexeditor
	hexedit --> Command line hexeditor
	termdown --> Displays a countdown in ascii art.
	classifier --> Organize files automatically in the current directory
	scholar.py --> Scrapper by http://twitter.com/ckreibich aka christian@icir.org
		for Google Scholar searching.
	feh --> Opens files very quickly and has most of what viewnior has.
	GeoIP --> look-up location of IP address
	traceroute --> traces the connection route from your computer to a server
	sensors --> monitor temperature
	wireless-tools --> tools for wireless LAN
	sc --> spreadsheet editor
	sc-im --> spreadsheet editot based on sc
	bison --> needed for sc-im
	units --> convert many different kinds of units of measurement
	moc --> command line music player
	movgrab --> grab video from websites
	mps-youtube --> Search YouTube and listen to videos or download them
	youtube-dl --> Download YouTube videos
	bashmount --> easy way to mount flash/hard drives and partitions via command line
	mpv --> allows mps-youtube to play audio in the command line
		*DirectFB --> For playing in TTY
		*DirectFB-Mesa
		*DirectFB-libSDL
	streamlink --> play live streams from command line
	irssi --> A popular commandline IRC client
	spell --> Adds spell-checking to nano
	
Step 4:
* Remove packages:
	notecase --> Too buggy in the encryption department
	ekiga --> qTox is better
	gftp --> FileZilla has more features, uses about the same RAM, and
		has updates; gftp hasn't been updated in years.
* This is an optional step and is the primary reason for making the 
	user run the Upgrade script one step at a time.
* Packages are only removed if said packages were determined as either
	useless, something better was found to replace, or problematic.

Step 5:
* Files are added at this step that cannot be via a normal package
	installation.
* This step is treated in the same manner as "Overlay Files" when
	building on SuseStudio.com

Step 6:
* Run scripts within Files folder
* Scripts here are not included within the Upgrade script in order to
	make it easier for any corrections that might be needed in the
	future or to add more.
	
Step 7:
* This step is used to correct files that are already installed on
	PsychOS.
* Updates for software that was originally compiled with source code
* Correct .desktop files
	- Replace cmus.desktop with mocp.desktop
* Correct icons
* Correct libraries or create necessary symbolic links

Step 8:
* Finds and install patches
* This very rarely does anything, but most people new to the zypper
	package management system aren't aware of it.
	
Step 9:
* Asks if you have 4 or more GB of RAM. This is important because 
	kernel-pae is not included with PsychOS by default; his means that
	if you have exactly 4 or more GB of RAM, your computer will only 
	access no more than 3 GB (typically) of RAM, even if you have 8 GB. 
	PsychOS is NOT a 64-bit system and therefore its default kernel is 
	non-PAE.
	
Step 10:
* Check for RootKits step
* This is only important if you want to make sure nothing compromising
	has happened to your system's security during the upgrade.
	Recommendations are made at the very end and web links provided to
	further explain what was found and how to improve or correct. Lynis
	was made for Unix-based systems such as Linux and Mac. IT IS NOT 
	100% ACCURATE. You can also run "rkhunter" in the terminal to search
	for RootKits.
	
Step 11:
* This pops up a menu with a list of suggested software to install that
	is not included but also not necessary for PsychOS's main purpose.
	Examples are software such as alternative web browsers, games, 
	emulators, and so forth. These are either installed via zypper, by
	downloading with wget and installing them from source, or placing the
	appropriate files where they need to be.
* Current offer list:
	Tor Browser --> Browse the internet anonymously
	Pale Moon --> Firefox alternative (uses older engine) but still updated
		regularly.
	Emulators --> A whole bunch of emulators picked based on age, reliability,
		and compatibility for popular computer and game systems most of 
		us either have experience with or at least appreciate to some degree.
		List (as would be typed in zypper):
			fceux --> NES
			zsnes --> SNES
			libmupen64plus2
			mupen64plus --> N64
			mupen64plus-*
			mupen64plus-plugin-video-glide64mk2
			mupen64plus-plugin-video-rice
			m64py --> GUI for Mupen64Plus
			pcsxr --> PS1
			vbam --> GBA (GBA4iOS compatible)
			vbam-lang
			xf86-input-joystick
			qjoypad --> GUI for keyboard to joystick options if having issues
			dosbox --> DOS emulator for playing games
	 Gourmet --> A recipe manager with lots of tools and plugins.
         Reditr --> A desktop Reddit client for Linux.
         PeaZip --> A free archive manager that supports 150+ archive types.
         VirtualBox --> Used to run an operating system while still in the current one.
         Corebird --> Gtk+ Twitter client
         JavE --> Convert pictures to ascii art or create from scratch.
         Xiphos --> Install Xiphos with various Bibles (Sword Project) for study.
         HubiC command line client --> A France-based cloud storage service.
         Tresorit client --> Encrypted cloud storage with Swiss privacy laws.
         Wireshark --> monitor network traffic
         lolcat --> cat command, but with a rainbow & animation options.
         xpaint --> 1994 styled paint editor; very lightweight and easy to use.
